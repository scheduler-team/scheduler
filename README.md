# Schedule Lab

ScheduleLab is an advanced schedule builder for college students, which generates the best possible schedules based on user's time preference, instructor rating, seats availability, etc. 

https://schedulelab.xyz/

## Project Components
- Angular Web App ([schedulelab-angular](https://github.com/schedulelab/schedulelab-angular))
- ASP.NET Core Backend ([schedulelab-dotnet](https://github.com/schedulelab/schedulelab-dotnet))
- Firebase (Auth, Database, and Hosting) ([Schedule Lab Firebase Console](https://console.firebase.google.com/u/0/project/scheduleru/))

## Project Management
### Milestones
https://gitlab.com/groups/scheduler-team/-/milestones
- Milestones are general goals and timelines

### Board
https://gitlab.com/groups/scheduler-team/-/boards
- The board is a centralized place to plan and track the working progress of specific features, bugs, etc.

# How to Contribute

## Thing you may need to know/learn

### Universal
You'd better know/learn the following before contributing to the project.
- OOP (Object-oriented programming)
- Git SCM (Source Control Management)
  - What is
    - Repository (repo)
    - Commit
    - Branch
  - How to
    - Clone a repo
    - Checkout a branch
    - Commit changes
    - Sync with the remote origin


### Front-end (Angular Web App)
You'd better know/learn some basics of the following before working on the UI part of the Angular Web App.
- HTML (Learn on [Sololearn](https://www.sololearn.com/Course/HTML/))
- CSS (Learn on [Sololearn](https://www.sololearn.com/Course/CSS/))

If you haven't worked with any client-side JavaScript Frameworks (like Angular, React, or Vue), you'd better check out one or both of the following tutorials first.
- [Getting started with a basic Angular app](https://angular.io/start)
- [Angular Tutorial: Tour of Heroes](https://angular.io/tutorial)

You can learn the following while contributing to the project.
- [Angular](https://angular.io/) with [TypeScript](https://www.typescriptlang.org/)
  - Components, Templates, and Data Binding
  - Routing
  - Services and Dependency Injection (DI)
  - Access Web API over HTTP
- [Angular Material](https://material.angular.io/)
- [Angular FlexLayout](https://github.com/angular/flex-layout)
- [AngularFire](https://github.com/angular/angularfire)

### Back-end (.NET Core)
You can learn the following while contributing to the project.
- C# and .NET
  - Generics
  - LINQ
  - Asynchronous Programming
- [ASP.NET Core](https://dotnet.microsoft.com/apps/aspnet)
  - API Controllers
  - HTTP Methods
  - Dependency Injection
- Unit Testing using [MSTest](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-mstest)

## Workflow

### Get Started for the First Time
- Read the README files for the front-end project ([scheduler-angular](https://gitlab.com/scheduler-team/scheduler-angular#scheduler-web-app)) and the back-end project ([scheduler-dotnet](https://gitlab.com/scheduler-team/scheduler-dotnet#scheduler-net))
- Setup your development environment according to the README file(s)

### Get Started on a Work Item
- Go to [team board](https://gitlab.com/groups/scheduler-team/-/boards)
- Decide the issue (work-item) you'd like to work on
  - Consider the items in `Todo` stage first
  - You can click each issue to see detailed  requirement and/or task list (items not yet in `Todo` may not have a task list)
  - Do NOT work on an issue that has an assignee already
- Click the issue you'd like to work on
- Click `Assign yourself` in the right panel
- Click `Create merge request` in the center
- A new branch and an associated merge request is automatically created for you
- Open the repository in your local dev environment
- Sync the repository with remote origin
- Checkout the new branch
- Start coding

### Coding for a Work Item
- Commit and push changes frequently
  - Use descriptive commit messages
- Add a tick to each task you completed on the issue page
- Check whether the CI pipeline succeeds for each commit you push
- Ask for specifications in the issue page if the task list is not detailed enough

### Finishing up a Work Item
- Resolve merge conficts (if any)
  - Only do it on your own when you know exactly how to resolve them
  - Request a code review if needed
- Resolve WIP (Work-in-progress) status on the merge request page
- Wait for review and merge approval
