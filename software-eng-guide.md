# ScheduleLab Software Engineering Guide

## Learning Path for Front-end

### Week 1 - HTML & CSS
  - [2 hours] Complete first 3 modules of HTML Course on [Sololearn](https://www.sololearn.com/Course/HTML/)
    - Module 1 - Overview
    - Module 2 - HTML Basics
    - Module 3 - Challenges
  - [2 hours] Complete first 3 modules of CSS Course on [Sololearn](https://www.sololearn.com/Course/CSS/)
    - Module 1 - The Basics
    - Module 2 - Working with Text
    - Module 3 - Properties

### Week 2 - JavaScript & Git
  - [2 hours] Complete first 6 modules of JavaScript Course on [Sololearn](https://www.sololearn.com/Course/JavaScript)
    - Module 1 - Overview
    - Module 2 - Basic Concepts
    - Module 3 - Conditions & Loops
    - Module 4 - Functions
    - Module 5 - Objects
    - Module 6 - Core Objects
  - [2 hours] Learn Git (try to find resources on your own!)
    - What is
      - Repository (repo)
      - Commit
      - Branch
    - How to
      - Clone a repo
      - Checkout a branch
      - Commit changes
      - Sync with the remote origin

### Week 3 - Angular with TypeScript (Part 1)
  - [1 hour] Setup Angular Development Environment (https://angular.io/guide/setup-local)
    - Install Node.js
    - Install Angular CLI
    - Create your first Angular Web App
    - Run your app
  - [3 hours] Complete first-half of Angular **Tour of Heroes** Tutorial (https://angular.io/tutorial)
    - Introduction
    - Create a project
    - 1 - The Hero Editor
    - 2 - Display a List
    - 3 - Create a Feature Component

### Week 4 - Angular with TypeScript (Part 2)
  - [4 hours] Complete the remaining parts of Angular **Tour of Heroes** Tutorial (https://angular.io/tutorial)
    - 4 - Add Services
    - 5 - Add In-app Navigation
    - 6 - (OPTIONAL) Get Data from a Server 

### Week 5 - Angular Material & Angular FlexLayout
  [4 Hours] Try to beautify your Tour of Heroes web app by using material design components and flex layout
  - [Angular Material](https://material.angular.io/)
    - [Getting Started](https://material.angular.io/guide/getting-started)
    - [Components](https://material.angular.io/components/categories)
  - [Angular FlexLayout](https://github.com/angular/flex-layout#angular-flex-layout)
    - [Getting Started](https://github.com/angular/flex-layout#getting-started)
    - [Layout Demos](https://tburleson-layouts-demos.firebaseapp.com/#/docs)