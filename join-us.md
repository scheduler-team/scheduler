# Join Schedule Lab!

Schedule Lab's mission is to build an advanced schedule builder for college students, which generates the best possible schedules based on a student's time preference, instructor rating, seats availability, etc. providing an easy hassle-free way to plan schedules. 

We are looking for highly motivated individuals who are passionate about building an impactful, innovative, production-ready product that could be used by college students nationwide. 

### General Timeline
- Jan 2020: Prototype
- Jul 2020: MVP
- Oct 2020: Insider Preview
- Jan 2021: Public Preview (Projected)

**Open Positions:** Design & Media | Outreach | Software Engineering

**Time Commitment:** 4 - 10 hours / week

**Apply for Positions:** https://forms.gle/uczADQzkydcCJhzC6

**Join Insider Preview Program:** https://forms.gle/nUT9WdR6WkyfRYff8

**Slack:** https://join.slack.com/t/schedulelab/shared_invite/zt-hktatej9-3gcpSLYMbIdcFUCFl24rIw

## Design & Media
### Responsibilities
  - Design logos and posters for the product and team
  - Design beautiful, interactive UI/UX for the web app
  - Create video advertisement

### Qualification
Minimum qualification: experience using any graphics or video editor

You'll be a strong candidate if you have experience with any of the following:
  - Professional graphics editor like Photoshop
  - Professional video editor like After Effect or  Premiere Pro
  - UI/UX prototyping tool like Figma

## Outreach
### Responsibilities
  - Work closely with users and insiders to collect feedbacks including bug reports, feature requests, etc
  - Manage social media and public Slack channels
  - Cooperate with university departments
  - Reach out to a diverse group of potential users

### Qualification
  - Strong communication skills, verbal and/or written

## Software Engineering
### Responsibilities
  - Build Web App with interactive UI/UX
  - Develop scheduling and schedule rating algorithms
  - Integrate course data and registration systems of colleges across US
  - Design Web API and course data provider interface

### Stacks we are using
  - Angular Web App (HTML, SCSS, TypeScript)
  - .NET Core Backend (C#)
  - Firebase (Auth & NoSQL Database)

### Qualification
Minimum qualification: object-oriented programming in any language

You will stand out if you have experience with any of the following: 

  - Git
  - HTML & CSS
  - JavaScript / TypeScript
  - Angular / React / Vue
  - REST API
  - SQL/NoSQL Database
  - ASP.NET Core / Spring / Django / Express
  - Firebase
  - Unit testing

We do not expect you to have any experience of the frameworks listed above especially if you are in your first or second year. We provide detailed guidance on how to get started with these frameworks. If you are passionate about creating projects that have a real impact and you are self-motivated to learn, we are looking forward to having you on board!

## Apply

Apply for Positions: https://forms.gle/uczADQzkydcCJhzC6

Join Insider Preview Program: https://forms.gle/nUT9WdR6WkyfRYff8

Slack: https://join.slack.com/t/schedulelab/shared_invite/zt-hktatej9-3gcpSLYMbIdcFUCFl24rIw
